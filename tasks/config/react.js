module.exports = function(grunt) {

  var templateFilesToInject = [
    'templates/**/*.jsx'
  ];
  console.log('loaded');
  grunt.config.set('react', {
    dev: {
      files: {
        // e.g.
        // 'relative/path/from/gruntfile/to/compiled/template/destination'  : ['relative/path/to/sourcefiles/**/*.html']
        '.tmp/public/react.jsx': templateFilesToInject
      }
    },
    dynamic_mappings: {
      files: [
        {
          expand: true,
          cwd: 'templates/',
          src: ['**/*.jsx'],
          dest: '.tmp/public/',
          ext: '.jsx'
        }
      ]
    }
  });

  grunt.loadNpmTasks('grunt-react');
};
