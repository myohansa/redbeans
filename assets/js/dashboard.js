function bind(){
	$('#assets-container').on('click','.btn-update', function(e) {
		e.preventDefault();
		var $this = $(e.target),
			$wrapper = $('#asset-wrapper'),
			assetID = $wrapper.data('asset-id'),
			$name = $wrapper.find('[name="name"]'),
			$description = $wrapper.find('[name="description"]');
		$.ajax("/dashboard/assets/",{
			data: {
				id: assetID,
				name: $name.val(),
				description: $description.val()
			},
			method: "PATCH"
		}, function(res){
			$name.val(res.name);
			$description.val(res.description);
		});

	});

	$('#pagination').on('click','.pager', function(e) {
		e.preventDefault();
		var $this = $(e.target),
			index = $this.data('page')-1;
		$('.pager').removeClass('active');
		$this.addClass('active');
		destroyPlayer();
		destroyAsset();
		window.page = index;
		$.when(renderAsset()).then(function() {
			setupPlayer();
		});

	});

  $('#assets-container').on('click','#get-embed-code', function(e){
    $('[name="embed_code"').val(videoPlayer.getEmbedCode());
  });
}

function getDuration(){
  var duration = moment.duration(videoPlayer.getDuration(),'seconds');
  return duration.get('minutes') + ":" + duration.get('seconds');
}

function destroyPlayer(){
	videoPlayer.destroy();
}

function setupPlayer(){
	$.when(window.videoPlayer = OO.Player.create(assets[page].embed_code, assets[page].embed_code, {height:600})).done( function() {
		window.videoPlayer.play();
		videoPlayer.mb.subscribe('*','example',function(eventName){
			$('#message-bus').append(eventName+"<br/>");
      $('#player-state').html(videoPlayer.getState());
      $('#player-duration').html(getDuration());
		});
	});
}

function destroyAsset(){
	$('#assets-container #asset-wrapper').remove();
}

function renderAsset(){
	var source = JST['assets/templates/assetsTemplate.html'],
		html = source(assets[page]);
	$('#assets-container').prepend(html);
}

function init(){
	$.getJSON('/dashboard/assets/', function(res) {
		var $container = $('#assets-container'),
			paginationSource = JST['assets/templates/pagination.html'],
			paginationHTML = "";

		window.assets = res.items;
		window.page = 0;

		$.when(renderAsset()).then(function() {
			_.each(assets, function(asset,id){
				paginationHTML += paginationSource({num: (id+1)});
			});

			$('#pagination').append(paginationHTML);
      $('#pagination .pager:eq(0)').addClass('active');
			setupPlayer();
		});

		bind();
	});

}

$(document).ready(function() {
	init();
});
