var crypto = require('crypto'),
	request = require('request-json'),
	ooyalaAPI = sails.config.ooyalaAPI;
module.exports = {
	request: function(){
		if(arguments.length < 3){
			throw new Error(200,"Insufficient number passed for ooyalaService.request()");
		}
		var method = arguments[0] || 'GET',
			path = arguments[1] || '',
			params = {},
			body = (!_.isFunction(arguments[2])) ? arguments[2] : {},
			callback = (arguments.length > 3 && _.isFunction(arguments[3])) ? arguments[3] : arguments[2];
			uri = ooyalaAPI.protocol + '//' + ooyalaAPI.host,
			options = {},
			client = request.newClient(uri);
		delete body['id'];
		console.log('this body',body);
		_.extend(params, {
			expires: this.getExpires(params.expires),
			api_key: ooyalaAPI.api_key
		});

		path += "?" + this.setRequestString(method,path,params,body);
		console.log(uri+path);
		switch(method){
			case "GET": 
				client.get(path, function(err,res,body) {
					console.log(body);
					return callback.call(this,body);
				});
				break;
			case "PATCH":
				client.patch(path, body, function(err, res, body2) {
					console.log(body);
					return callback.call(this,body2);
				})
		}		
	},
	/**
	 * Get expiry for API 
	 **/
	getExpires: function(expires){
		expires = expires || (1000*60*10);
		var now = new Date();
		return (now.getTime() + expires);
		//return 1299991855;
	},

	getSignature: function(method,path,params,body){
//		console.log('params in sign', requestString);
		var params = params || "",
			requestString = "",
			extra = "",
			paramsKeys = _.keys(params).sort(); 
		
		_.each(paramsKeys, function(key) {
			requestString += key + "=" + params[key];
		});
		if(!_.isEmpty(body) && !_.isUndefined(body)) {
			requestString += JSON.stringify(body);
		}
		
		return encodeURIComponent( 
			String(crypto.createHash("sha256").update(ooyalaAPI.api_secret + method + path + requestString).digest("base64")).substring(0,43).match(/.*[^=]/g)[0]
		);
	},

	setRequestString: function(method,path,params,body) {
		params = params || {};
		var data = [];

		 _.extend(params, {
			signature: this.getSignature(method,path,params,body) 
		});
		
		_.each(params, function(value,key) {
			if(!_.isEmpty(value) || _.isNumber(value)) {
				data.push( key + '=' + value );
			} 
		});
		return data.join("&");
	}
}