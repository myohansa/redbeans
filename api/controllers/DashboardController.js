/**
 * DashboardController
 *
 * @description :: Server-side logic for managing dashboards
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var crypto = require('crypto');
module.exports = {
	index: function(req, res) {
		return res.view('dashboard/index');
	},
	assets: function(req, res) {
		var id = (_.isUndefined(req.param('id'))) ? "" : "/" + req.param('id');
		console.log(req.allParams());
		ooyalaService.request(req.method,'/v2/assets' + id, req.allParams(), function(data){
			//console.log("DATA:", data, req.method);
			return res.json(data);
		});
	},
	players: function(req, res){
		var id = (_.isUndefined(req.param('id'))) ? "" : "/" + req.param('id');
		ooyalaService.request(req.method,'/v2/players' + id, req.allParams(), function(data){
			return res.json(data);
		});
	}
};

